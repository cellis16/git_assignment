//Code to calculate factorial of a number
//Load Scanner library to use Scanner function
import java.util.Scanner;
//The main class 
//Factorial class for calculating factorial of an integer 
class Factorial
{
   public static void main(String args[])
   {
      int n, c, fact = 1;
 
      System.out.println("Enter an integer to calculate it's factorial");
      //Read in input character for procssessing
      Scanner in = new Scanner(System.in);
 
      n = in.nextInt();
 //Test end case
 //Invalid input: print error message

      if ( n < 0 )
         System.out.println("Number should be non-negative.");
      else
      {

//Actual logic for calculating factorial
//Valid input: calculate factorial

         for ( c = 1 ; c <= n ; c++ )
            fact = fact*c;
 
         System.out.println("Factorial of "+n+" is = "+fact);
      }
   }
}
